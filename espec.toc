\select@language {brazilian}
\select@language {english}
\select@language {brazilian}
\contentsline {chapter}{1~Introdu\IeC {\c c}\IeC {\~a}o}{11}{CHAPTER.1}
\contentsline {chapter}{2~Estado da Arte}{12}{CHAPTER.2}
\contentsline {section}{\numberline {2.1}Grafos}{12}{section.2.1}
\contentsline {section}{\numberline {2.2}APIs}{12}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}D3.js}{12}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Sigma.js}{14}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Cytoscape.js}{15}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Vis.js}{17}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}VivaGraph.js}{17}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}JSNetworkX}{18}{subsection.2.2.6}
\contentsline {subsection}{\numberline {2.2.7}APIs descontinuadas}{18}{subsection.2.2.7}
\contentsline {chapter}{3~Avalia\IeC {\c c}\IeC {\~a}o}{19}{CHAPTER.3}
\contentsline {section}{\numberline {3.1}Crit\IeC {\'e}rios de avalia\IeC {\c c}\IeC {\~a}o}{19}{section.3.1}
\contentsline {section}{\numberline {3.2}Avalia\IeC {\c c}\IeC {\~a}o geral}{20}{section.3.2}
\contentsline {section}{\numberline {3.3}Resumo}{21}{section.3.3}
\contentsline {chapter}{4~Estudo de caso}{24}{CHAPTER.4}
\contentsline {section}{\numberline {4.1}Estudo}{24}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Cytoscape.js}{24}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}D3.js}{26}{subsection.4.1.2}
\contentsline {chapter}{5~Conclus\IeC {\~a}o}{28}{CHAPTER.5}
\contentsline {chapter}{\xspace {}Refer{\^e}ncias}{29}{schapter.8}
\contentsline {chapter}{\MakeUppercase {Ap{\^e}ndice} A~--- Exemplo de implementa\IeC {\c c}\IeC {\~a}o usando Cytoscape}{31}{CHAPTER.A}
\contentsline {chapter}{\MakeUppercase {Ap{\^e}ndice} B~--- Exemplo de implementa\IeC {\c c}\IeC {\~a}o usando D3}{34}{CHAPTER.B}
