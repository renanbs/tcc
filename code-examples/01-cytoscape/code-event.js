$(function()
{
	var cy = cytoscape({
		container: document.getElementById('cy'),
		style: cytoscape.stylesheet()
		.selector('node')
		.css({
			'content': 'data(name)',
			'background-color': '#6272A3',
			'shape': 'pentagon',
		})
		.selector('edge')
		.css({
			'target-arrow-shape': 'triangle',
			'width': 4,
			'line-color': '#61bffc',
			'target-arrow-color': '#61bffc',
			'curve-style': 'bezier'
		}),
		elements: {
			nodes: [
				{ data: { id: 'a', name: 'Presidente' } },
				{ data: { id: 'b', name: 'Superintendente TI' } },
				{ data: { id: 'c', name: 'Gerente TI' } },
				{ data: { id: 'd', name: 'Coordenador TI' } },
				{ data: { id: 'e', name: 'Especialista TI' } },
				{ data: { id: 'f', name: 'Analista TI Senior' } },
				{ data: { id: 'g', name: 'Analista TI Pleno' } },
				{ data: { id: 'h', name: 'Analista TI Junior' } },
				{ data: { id: 'i', name: 'Estagiario' } }
			],
			edges: [
				{ data: { id: 'ab', weight: 1, 
					source: 'a', target: 'b' } },
				{ data: { id: 'bc', weight: 2, 
					source: 'b', target: 'c' } },
				{ data: { id: 'cd', weight: 3, 
					source: 'c', target: 'd' } },
				{ data: { id: 'de', weight: 5, 
					source: 'd', target: 'e' } },
				{ data: { id: 'df', weight: 6, 
					source: 'd', target: 'f' } },
				{ data: { id: 'dg', weight: 7, 
					source: 'd', target: 'g' } },
				{ data: { id: 'dh', weight: 8, 
					source: 'd', target: 'h' } },
				{ data: { id: 'di', weight: 9, 
					source: 'd', target: 'i' } }
			]
		},
		layout: {
			name: 'breadthfirst',
			directed: true,
			roots: '#a',
			padding: 10,
			animate: true,
			animationDuration: 500
		}
	});
	
// 	cy.on('tap', 'node', { foo: 'bar' }, function(evt){
// 		console.log( evt.data.foo ); // 'bar'
// 
// 		var node = evt.cyTarget;
// 		console.log( 'tapped ' + node.id() );
// 	});
	
	cy.on('tap', function(event)
	{
		var evtTarget = event.cyTarget;

		if( evtTarget === cy ){
			console.log('Clique no fundo');
		} else {
			console.log('Clique em um elemento');
		}
	});
	
	
});
