var width = 910,
	height = 400;

var tree = d3.layout.tree().size([height, width - 160]); (*@\label{line:d3_type}@*)

var diagonal = d3.svg.diagonal()
	.projection(function(d) { 
		return [d.y, d.x]; 
	});

var svg = d3.select("body").append("svg")
	.attr("width", width)
	.attr("height", height)
	.append("g")
	.attr("transform", "translate(40,0)");

d3.json("data.json", function(error, json) { (*@\label{line:d3_load_json}@*)
if (error) 
	throw error;

var nodes = tree.nodes(json), (*@\label{line:d3_load_in}@*)
	links = tree.links(nodes);

var link = svg.selectAll("path.link")
	.data(links) (*@\label{line:d3_links}@*)
	.enter().append("path")
	.attr("class", "link")
	.attr("d", diagonal);

var node = svg.selectAll("g.node")
	.data(nodes) (*@\label{line:d3_nodes}@*)
	.enter().append("g")
	.attr("class", "node")
	.on("click", function () { console.log("Hello"); }) (*@\label{line:d3_event}@*)
	.attr("transform", function(d) { 
		return "translate(" + d.y + "," + d.x + ")"; 
	});

node.append("circle")
	.attr("r", 4.5);

node.append("text") (*@\label{line:d3_pos}@*)
	.attr("dx", function(d) { 
		return d.children ? 20 : -20; 
	
	})
	.attr("dy", -10)
	.attr("text-anchor", function(d) { 
		return d.children ? "end" : "start"; 
		
	})
	.text(function(d) { 
		return d.name; 
	});
});

d3.select(self.frameElement).style("height", height + "px");


